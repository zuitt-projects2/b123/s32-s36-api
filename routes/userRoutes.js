const express = require('express')
const router = express.Router();
const auth = require('../auth')

//verify is a method from our auth module which will let us verify if the jst is legitimate and let us decode the payload
const {verify} = auth

const userControllers = require('../controllers/userControllers')

const
{
	createUserController,
	loginUser,
	getSingleUserController,
	updateProfileController,
	enrollController,
	checkEmailExistsController,
	getEnrollmentsController
} = userControllers
//add User
router.post('/',createUserController)

//login User
router.post('/login',loginUser)

//getSingleUser details
//route methods, much like middlewares, give access to request,response and next objects for the functions that are included in them
// In fact, in expressjs, we can add multiple layers of middleware to do tasks before letting another function perform another task
router.get('/getUserDetails',verify,getSingleUserController)

//update the logged in user (profile)
router.put('/updateProfile',verify,updateProfileController)
router.post('/enroll',verify,enrollController)

//check email exist route
router.post('/checkEmailExists',checkEmailExistsController)

//get enrollees
router.get('/getEnrollments',verify,getEnrollmentsController)
module.exports = router