const express = require('express')
const router = express.Router();
const auth = require('../auth')
const {verify,verifyAdmin} = auth
const courseControllers = require('../controllers/courseControllers')
const{
	createCourseController,
	getAllCourseController,
	getActiveCourseController,
	getSingleCourseController,
	updateCourseController,
	archiveCourseController,
	activateCourseController,
	getEnrolleesController
} = courseControllers

router.post('/',verify,verifyAdmin,createCourseController)
router.get('/',verify,verifyAdmin,getAllCourseController)
router.get('/getActiveCourses',verify,getActiveCourseController)
router.get('/getSingleCourse/:id',verify,getSingleCourseController)
router.put('/:id',verify,verifyAdmin,updateCourseController)
router.put('/archive/:id',verify,verifyAdmin,archiveCourseController)
router.put('/activate/:id',verify,verifyAdmin,activateCourseController)
router.get('/getEnrollees/:id',verify,verifyAdmin,getEnrolleesController)
module.exports = router