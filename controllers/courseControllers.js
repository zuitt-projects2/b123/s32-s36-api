const Course = require('../models/Course')
const User = require('../models/User')
const auth = require('../auth')
const {createAccessToken} = auth

module.exports.createCourseController = (request,response)=>{

	let newCourse = new Course ({

		name:request.body.name,
		description:request.body.description,
		price:request.body.price,

	})

	//always return your response.send()
	//response.send() should alywas be the last process done. No other steps within the code block should be done after response.send()
	newCourse.save()
	.then(result => response.send(result))
	.catch(err => response.send(err))

}

module.exports.getAllCourseController = (request,response) => {

	Course.find()
	.then(result => {
		if(result !== null){
			return response.send((result));
		} else {
			return response.send({message:"No Course Found"})
		}
	})
	.catch(err => response.send(err))
}

module.exports.getActiveCourseController = (request,response) => {
	Course.find({isActive:true})
	.then(result => response.send(result))
	.catch(err => response.send(err))
}

module.exports.getSingleCourseController = (request,response) => {

	Course.findById(request.params.id)
	.then(result => response.send(result))
	.catch(err => response.send(err))
}

module.exports.updateCourseController = (request,response) => {

	let updates = {

	 	name:request.body.name,
	 	description:request.body.description,
	 	price:request.body.price
	}

	 Course.findByIdAndUpdate(request.params.id,updates,{new:true})
	 .then(updatedCourse => response.send(updatedCourse)).catch(err => response.send(err))
}

module.exports.archiveCourseController = (request,response) => {
 
	 Course.findByIdAndUpdate(request.params.id,{isActive : false},{new:true})
	 .then(archiveCourse => response.send(archiveCourse)).catch(err => response.send(err))
}

module.exports.activateCourseController = (request,response) => {

	 Course.findByIdAndUpdate(request.params.id,{isActive:true},{new:true})
	 .then(activateCourse => response.send(activateCourse)).catch(err => response.send(err))
}

module.exports.getEnrolleesController = (request,response) => {
	
	Course.findById(request.params.courseId)
		.then(result => response.send(result.enrollments))
		.catch(err => response.send(err))
}