const User = require('../models/User')
const Course = require('../models/Course')

//bcrypt has methods which will help us add a layer of security for our user's password
const bcrypt = require('bcrypt')

//import the auth module and deconstruct to get our createAccessToken method
const auth = require('../auth')
const {createAccessToken} = auth

module.exports.createUserController = (request,response)=>{

	console.log(request.body);
	if(request.body.password.length < 8) return response.send({message :"Password is too short."})
	/*
		brcypt adds a layer of security to our passwords.

		what bcrypt does is hash our password string into a randomized character version of your password.

		it is able to hide your password within that randomized string.

		syntax:
		bcrypt.hashSync(<stringToBeHashed>,<salt-Rounds>)

		Salt-Rounds are the number of times the characters in the hash are randomized

	*/

	const hashedPW = bcrypt.hashSync(request.body.password,10);
	console.log(hashedPW)

	let newUser = new User({

		firstName:request.body.firstName,
		lastName:request.body.lastName,
		email:request.body.email,
		password:hashedPW,
		mobileNo:request.body.mobileNo

		})

	newUser.save()
	.then(result => response.send(result))
	.catch(err => response.send(err))

}

module.exports.loginUser = (request,response) => {

	/*
		1. Find the user by the email.
		2. If there is a found user, we will now check his password.
		3. if there is no found user, we will send a message to client.
		4. if upon checking the found user's password is correct we will generate a "key" to access our app.
		if not, we will turn him away by sending a message to the client.
	*/
	User.findOne({email : request.body.email})
	.then(result => {
		if(result === null){
			return response.send({message:"No User Found"})
		} else {
			/*console.log(request.body.email)
			console.log(request.body.password)*/
			//if we find a user, result will contain the found user's document:
			const isPasswordCorrect = bcrypt.compareSync(request.body.password,result.password)
			console.log(isPasswordCorrect)
			/*
				bcrypt.compareSync(<string>,<hashedString>)

				returns a boolean after comparing the first argument (string) and the hashed version of that string.
				If it matches, it will return true if not  returns false.

				In our booking systems's login, we compare the input password and the hashed password from our database.
			*/

			if(isPasswordCorrect){

				//console.log("We will generate a key.")
				return response.send({accessToken:createAccessToken(result)});

			} else {
				return response.send({message:"Password is incorrect"})
			}
		}
	})
	.catch(err => response.send(err))
}

module.exports.getSingleUserController = (request,response) => {

	//logged in user's details after decoding with auth module's verify()
	console.log(request.user)
	User.findById(request.user.id)
	.then(result => response.send(result))
	.catch(err => response.send(err))
}

module.exports.updateProfileController = (request,response) => {

	 //How can we get the logged in user's id? request.user
	 console.log(request.user)

	 //find the user by its id and update it
	 let updates = {

	 	firstName:request.body.firstName,
	 	lastName:request.body.lastName,
	 	mobileNo:request.body.mobileNo
	 }

	 User.findByIdAndUpdate(request.user.id,updates,{new:true})
	 .then(updatedUser => response.send(updatedUser)).catch(err => response.send(err))
}


/*
	async and await

		JS doesn't wait for your variable/functions to finish the process before continuing. it will either stop the whole process or just continue forward.

		with the use of async and await we can wait for the process to finish to finish before we go forward 

		we add async keyword to make a function asynchronous.

		await can only be used on async functions.

*/


module.exports.enrollController = async (request,response) => {


	if (request.user.isAdmin) return response.send({
		auth : "Failed",
		message:"Action Forbidded"})

/*
	Enrollment steps:

	1. Look for our user.
		Push the details of the course we're trying to enroll in our user's enrollment subdocument array. save () the user document and return true to a variable if saving is successful, return the error message if we catch an error otherwise

	2. Look for out course.
		push the details of the user we're trying to enroll in our courses' enrolless's subdocument array. save () the user document and return true to a variable if saving is successful, return the error message if we catch an eroor otherwise.

	3. When both saving of document are successful, we send a message to the client.

*/

let isUserUpdated = await User.findById(request.user.id).then(user => {
	/*console.log(user.enrollments)*/

	user.enrollments.push({courseId:request.body.courseId})

	return user.save()
	.then(user => true)
	.catch(err => err.message)
	})

	console.log(isUserUpdated)
	//End the request/response when isUserUpdated does not return true.
	if(isUserUpdated !== true) return response.send(isUserUpdated)

	let isCourseUpdated = await Course.findById(request.body.courseId).then(course => {

		course.enrollees.push({userId: request.user.id})

		return course.save()
		.then(user => true)
		.catch(err => err.message)
	})
	console.log(isCourseUpdated)
	if(isCourseUpdated !== true) return response.send(isCourseUpdated);

	if(isUserUpdated && isCourseUpdated) return response.send ("Enrolled Successfully.")
}

module.exports.checkEmailExistsController = (request,response) => {

	User.findOne({email:request.body.email}).then(result => {
		if(result === null) {
			return response.send({
				isAvailable: true,
				message:"Email Available."})
		} else {
			return response.send({
				isAvailable:false,
				message:"Email is already registered"})
		}
	}).catch(err => response.send(err))
}

module.exports.getEnrollmentsController = (request,response) => {

	User.findById(request.user.id)
	.then(result => response.send(result.enrollments))
	.catch(err => response.send(err))
	
}