/*We will create a module which will have functions and methods that will help us to authenticate our users to either give them permission or restriction from an action. To do this first we need to give our users a key to access our app*/

const jwt = require ('jsonwebtoken');
const secret = "CourseBookingApi"

/*
	JWT is a way to securely pass information from a part of a server to the frontend or other parts of the server. It allows us to create a sort of "keys" which is able to authenticate our user.

	The secret passed is any string but to allow access with the token, the secret must be intact.

	JWT is like a gift wrapping service but with a secret and only our system knows about this secret. and if secret is not intact or the JWT seems tampered with we will be able to reject the user who used an illegitimate token.

	this will ensure that only registered users can do certain things in our app.
*/

module.exports.createAccessToken = (user) => {
	//console.log(user)
	//data object is created to contain some details of our user.
	//only a logged in user is handed or given a token

	const data = {
		id:user._id,
		email:user.email,
		isAdmin:user.isAdmin
	}

	//create your jwt with the payload, with the secret, the algorithm to create your JWT
	return jwt.sign(data,secret,{})
}

module.exports.verify = (request, response, next) => {

	//tokens passed as bearer tokens can be found in which part of the request? headers 
	//Token variavle will hold our bearer token from our client.
	let token = request.headers.authorization
	/*console.log(token)*/
	//if no token is passed, request.headers.authorization
	if(typeof token === "undefined"){
		return response.send({auth:"Failed. No token"})
	} else {
		//extract the token and remove the "bearer" from our token variable.
		token = token.slice(7,token.length);

		//verify the legitimacy of your token:
		jwt.verify(token,secret,function(err,decodedToken) {
			
			//err will contain the error from our decoding our token.
			//decodeToken is our token after completing and accomplishing verification of its legitimacy against our secret.
			if(err){
				return response.send({auth:"Failed.", message: err.message})
			} else {
				console.log(decodedToken)
				//we will add a new property in the req object called user. assign the decoded data to that property.

				request.user = decodedToken;

				//next() will allow us to run the next function.(another Middleware or the controller)
				next()
			}
		})
	}
}

module.exports.verifyAdmin = (request,response,next) => {

	if(request.user.isAdmin) {
		next()
	} else {
		return response.send({

			auth:"Failed",
			message:"Action Forbidden"

		})
	}
}